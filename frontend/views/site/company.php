<?php
/**
 * Created by PhpStorm.
 * User: miloslawsky
 * Date: 10.03.17
 * Time: 16:11
 *
 * @var $model \common\models\Api
 */
?>
<div class="page__inner page__inner_index page__inner_company">
	<div data-scroll-speed="10" class="js-scroll-column page__left">
		<?= $this->renderFile('@frontend/views/layouts/default-left.php', ['active_api_id' => $model->id]) ?>
	</div>
	<div class="js-scroll-column page__right page__right_sticky">
		<div class="card-wrapper">
			<div class="company-passport">
				<div class="company-passport__logo">
					<img src="<?= $model->thumbnail_base_url.'/'.$model->thumbnail_path ?>">
				</div>
				<div class="company-passport__raiting">
					<span class="company-passport__title">Рейтинг АСН</span>
					<span class="company-passport__rate"><?= $model->rate_asn ?></span>
				</div>
				<div class="company-passport__raiting">
					<span class="company-passport__title">Рейтинг Эксперта РА</span>
					<span class="company-passport__rate"><?= $model->rate_expert ?></span>
				</div>
			</div>
			<div class="company-review">
				<h1><?= $model->name ?></h1>
				<?= $model->description ?>
			</div>
			<div class="clearfix"></div>
			<div class="company-files">
				<ul class="company-files__col">
					<?php foreach($model->files as $file){ ?>
						<div class="company-files__item">
							<div class="company-files__type">
								<svg class="icon icon_pdf">
									<use xlink:href="#m-pdf"></use>
								</svg>
							</div>
							<div class="company-files__title">
								<a href="<?= $file->file_base_url.'/'.$file->file_path ?>"><?= $file->name ?></a>
							</div>
						</div>
					<?php } ?>
				</ul>
			</div>
			<div class="services-info">
				<div class="service-center">
					<div class="service-center__title">Сервисный центр</div>
					<div class="service-center__name">
						<?= $model->service_center_url ?>
					</div>
				</div>
				<div class="service-phones">
					<div class="service-phones__title">Телефонные номера</div>
					<div class="service-phones__inner">
						<table class="service-phones__phones">
							<tbody>
								<?php foreach($model->phones as $phone) {?>
									<tr>
										<td><?= trim($phone->name) ?>:</td>
										<td><?= trim($phone->phone) ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>


