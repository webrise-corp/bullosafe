<?php
/**
 * Copyright (c) kvk-group 2017.
 */

namespace frontend\controllers;

use common\components\ApiModule;
use common\components\Calculator\filters\params\travel\FilterParamPrototype;
use common\components\Calculator\forms\prototype;
use common\models\Api;
use common\models\GeoCountry;
use common\models\Orders;
use common\models\ProgramResult;
use common\models\User;
use common\components\Calculator\filters\Filter;
use common\components\Calculator\forms\TravelForm;
use common\modules\ApiAlphaStrah\models\Country;
use common\modules\ApiAlphaStrah\models\Price;
use common\modules\ApiAlphaStrah\models\StruhSum;
use common\modules\ApiAlphaStrah\Module;
use frontend\models\PartnerForm;
use frontend\models\PersonInfo;
use Yii;
use frontend\models\ContactForm;
use yii\base\Object;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\httpclient\Client;

/**
 * Site controller - сборная солянка действий которыне затруднительно или безсмысленно выносить в отдельные контроллеры
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    /**
     * Тестовое действие
     */
    public function actionT(){
		/** @var $module Module */

		/*
		$soap = new \SoapClient('https://aisws.ingos.ru/sales-test/SalesService.svc?wsdl');

		$login_resp = $soap->Login([
			'User' => 'БУЛЛО СТРАХОВАНИЕ WS',
			'Password' => 'qk7p8cvj'
		]);
		if ($login_resp->ResponseStatus->ErrorCode==0 ){
			$session_token = $login_resp->ResponseData->SessionToken;


			$dicti = $soap->GetDicti([
				'SessionToken' => $session_token,
				'Product' => '13349216'
			]);

			//Yii::$app->response->format = Response::FORMAT_XML;
			//echo $dicti->ResponseData->any;

			VarDumper::dump( $dicti, 16, true);

		} else {
			var_dump($login_resp);
			die();
		}
		*/


		/*

		$url = "https://rgstest.virtusystems.ru/login.aspx";

		$post_data = json_encode(array("userName"=>"rgsUser", "password"=>"123456", "createPersistentCookie"=>false));

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		$data = curl_exec($ch);


		$module  = Yii::$app->getModule('ApiLiberty');

		$data = $module->testOrder();
		*/
		/*
		$module = Yii::$app->getModule('ApiTinkoff');
		$data = $module->testCalcQuote();
		*/

		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
		]);

		$header = array(
			'userLogin' => "partner-api",
			'userToken'  => "NAWaIwjvoToSUaTNplfZBMTFgN6goBO9o6c%2BAvKlff8mRM8bsGwOQ",
			'templateID' => 103,
			'subTemplate' => null
		);

		$data = array(
			"dateBeginTravel" => "2017-12-26",
			"dateEndTravel" => "2017-12-30",
			"insTerritory" => "00001",
			"insProgram" => "00001",
			"isYearContract" => false,
			"countOfDays" => 15,
			"typeYearContract" => "00001",
			"insuredGroup1" => 1,
			"insuredGroup2" => 1,
			"insuredGroup3" => 1,
			"isOptionSport" => false,
			"isOptionBaggage" => false,
			"isOptionSpecialCase" => false,
			"isOptionLawyer" => false,
			"isOptionPrudent" => false
		);

		$response = $client->createRequest()
			->setHeaders($header)
			->setMethod('post')
			->setUrl("https://dev.sberbankins.ru/ws/rest/loader/calcObject")
			->setData($data)
			->send();

		if ($response->isOk) {
			$data = $response->data;
		} else $data = $response->statusCode;

		VarDumper::dump( $data, 16, true);
    }

    /**
     * Тестовое действие
     */
    public function actionT2()
	{
		$soap = new \SoapClient('https://aisws.ingos.ru/sales-test/SalesService.svc?wsdl',['trace'=>true]);

		$login_resp = $soap->Login([
			'User' => 'БУЛЛО СТРАХОВАНИЕ WS',
			'Password' => 'qk7p8cvj'
		]);
		if ($login_resp->ResponseStatus->ErrorCode==0 ){
			$session_token = $login_resp->ResponseData->SessionToken;


			$TariffParameters = (object)[];
			$TariffParameters->Agreement = (object)[];
			$TariffParameters->Agreement->PRODUCT = '13349216';
			$TariffParameters->Agreement->CURCODE = 'USD';
			$TariffParameters->Agreement->DATEBEG = \DateTime::createFromFormat('d.m.Y','20.10.2017')->format('Y-m-d');
			$TariffParameters->Agreement->DATEEND = \DateTime::createFromFormat('d.m.Y','22.10.2017')->format('Y-m-d');
			$TariffParameters->Agreement->TERRITORY_LIST = (object)[];
			$TariffParameters->Agreement->TERRITORY_LIST->TERRITORY = '5222628903';
			$TariffParameters->Agreement->COVER = (object)[];
			$TariffParameters->Agreement->COVER->MEDICAL = (object)[];
			$TariffParameters->Agreement->COVER->MEDICAL->LimitSum = '15000';
			$TariffParameters->Agreement->INSURED_LIST = (object)[];
			$TariffParameters->Agreement->INSURED_LIST->INSURED = (object)[
				'NAME' => 'TEST',
				'BIRTHDATE' => \DateTime::createFromFormat('d.m.Y','01.01.1980')->format('Y-m-d')
			];

			$params = [
				'SessionToken' => $session_token,
				'TariffParameters' => $TariffParameters
			];

			$resp = $soap->GetTariff((object)$params);

			//echo $soap->__getLastRequest();
			//Yii::$app->response->format = Response::FORMAT_XML;
			var_dump($resp);
		} else {
			var_dump($login_resp);
			die();
		}
    }

    /**
     * Заглавная страница
     * @return string
     */
    public function actionIndex()
    {
       return $this->render('../page/calc');
    }

    /**
     * @param $id
     *
     * @return string
     * @throws HttpException
     */
    public function actionCompany($id){
    	$model = Api::findOne(['id' => $id]);
    	if ($model) {
    		return $this->render('company', ['model' => $model]);
	    } else {
    		throw new HttpException(404, 'Карточка компании не найдена');
	    }
    }

    /**
     * Действие Партнерам
     */
    public function actionSendPartner() {
		$send = false;
		try {
			$model = new PartnerForm();
			if ($model->load(Yii::$app->request->post())) {
				if ($model->validate() && $model->send('evgeniya.khityaeva@gmail.com')) {
					$send = true;
				}
			}
		} catch (Throwable $t) {

		} catch (Exception $e) {

		}

		if ($send) {
			echo json_encode(['res'=>1, 'msg'=>\Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.')]);
		} else {
			echo json_encode(['res'=>0, 'msg'=>\Yii::t('frontend', 'There was an error sending email.')]);
		}

	}
}
