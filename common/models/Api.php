<?php

namespace common\models;

use common\components\ApiModule;
use common\components\Calculator\forms\TravelForm;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\base\Module;
use yii\httpclient\Exception;

/**
 * API страховых компаний
 * This is the model class for table "api".
 *
 * @property integer $id
 * @property string $name
 * @property string $class
 * @property string $rate_expert
 * @property string $rate_asn
 * @property integer $enabled
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $actions
 * @property string $description
 * @property string $service_center_url
 *
 * @property ApiPhone[] $phones
 * @property ApiFiles[] $files
 */
class Api extends \yii\db\ActiveRecord
{
	/**
     * Логотип
	 * @var array
	 */
	public $thumbnail;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'class', 'rate_expert', 'rate_asn'], 'required'],
            [['id', 'enabled'], 'integer'],
            [['name', 'class', 'service_center_url'], 'string', 'max' => 255],
            [['rate_expert', 'rate_asn'], 'string', 'max' => 50],
	        [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
	        [['actions', 'description'], 'string'],
	        [['thumbnail'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'class' => 'Класс модуля',
            'rate_expert' => 'Рейтинг эксперта',
            'rate_asn' => 'Рейтинг АСН',
            'enabled' => 'Разрешено',
            'thumbnail' => 'Логотип',
            'actions' => 'Действия при страховом случае',
            'description' => 'Описание',
            'service_center_url' => 'Код ссылки на сервисный центр',
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => UploadBehavior::className(),
				'attribute' => 'thumbnail',
				'pathAttribute' => 'thumbnail_path',
				'baseUrlAttribute' => 'thumbnail_base_url'
			]
		];
	}

    /**
     * Возвращает массив для генерации административного меню
     * @return array
     */
    public static function getAdminMenu(){
		$menu = [
			'label'=>Yii::t('backend', 'API'),
			'icon'=>'<i class="fa fa-address-book"></i>',
			'url' => '#',
			'items'=>[]
		];
		$models = Api::find()->where(['enabled' => 1])->orderBy(['name' => SORT_ASC])->all();
		foreach ($models as $model){
			$className = $model->class;
			if (class_exists($className) && method_exists($className, 'getAdminMenu')){
				$menu['items'][] = call_user_func(array($className, 'getAdminMenu'));
			}
		}
		return $menu;
	}

	/**
     * Телефоны
	 * @return \yii\db\ActiveQuery
	 */
	public function getPhones()
	{
		return $this->hasMany(ApiPhone::className(), ['api_id' => 'id']);
	}

	/**
     * Файлы
	 * @return \yii\db\ActiveQuery
	 */
	public function getFiles()
	{
		return $this->hasMany(ApiFiles::className(), ['api_id' => 'id']);
	}

	/**
     * Возвращет класс модуля API
	 * @return ApiModule
	 * @throws Exception
	 */
	public function getModule(){
	    $className = $this->class;
	    if (class_exists($className)){
		    return new $className($className);
	    }
		throw new Exception("Api module {$this->name} not implemented", 501);
    }

    /**
     * Поиск программ по заданным критериям
     * @param TravelForm $form Критерии поиска
     *
     * @return ProgramResult|null
     * @throws Exception
     */
    public function search(TravelForm $form){
    	$module = $this->getModule();
    	if ($module){
		    return $module->search($form);
	    }
	    throw new Exception("Method search not implemented in api module {$this->name}", 501);
    }

    /**
     * Возвращает номера телефонов в виде массива, где ключ - название, значение - телефон
     * @return string[]
     */
    public function getPhonesAsArray(){
    	$res = [];

    	foreach($this->phones as $phone){
    		$res[$phone->name] = $phone->phone;
	    }

	    return $res;
    }
}
