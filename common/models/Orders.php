<?php
/**
 * Copyright (c) kvk-group 2017.
 */

namespace common\models;

use baibaratsky\yii\behaviors\model\SerializedAttributes;
use common\components\SerializeBehavior;
use common\components\Calculator\forms\TravelForm;
use Yii;

/**
 * Заказы
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $api_id
 * @property string $price
 * @property integer $currency_id
 * @property string $status
 * @property integer $holder_id
 * @property integer $user_id
 * @property array $info
 * @property TravelForm $calc_form
 * @property object $program
 * @property string $created_at
 * @property integer $is_police_downloaded
 *
 *
 * @property Api $api
 * @property Currency $currency
 * @property Person $holder
 * @property User $user
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * Статус - новый
     */
    const STATUS_NEW = 'new';
    /**
     * Статус - оплачен
     */
    const STATUS_PAYED = 'payed';
    /**
     * Статус - оплачен и подтверждён
     */
    const STATUS_PAYED_API = 'payed_api';
    /**
     * Статус - расчет
     */
    const STATUS_CALC = 'calc';

    /**
     * Названия статусов
     * @var array
     */
    public $_status_names = [
		self::STATUS_NEW => 'Новый',
		self::STATUS_PAYED => 'Оплачен',
                self::STATUS_PAYED_API => 'Оплачен и подтверждён',
		self::STATUS_CALC => 'Предварительный расчет',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['api_id', 'price', 'currency_id', 'holder_id', 'info', 'calc_form', 'program'], 'required'],
            [['api_id', 'currency_id', 'holder_id', 'is_police_downloaded'], 'integer'],
            [['price'], 'number'],
            [['status'], 'string'],
	        [['status'], 'in', 'range' => [self::STATUS_NEW, self::STATUS_PAYED, self::STATUS_CALC, self::STATUS_PAYED_API]],
            [['api_id'], 'exist', 'skipOnError' => true, 'targetClass' => Api::className(), 'targetAttribute' => ['api_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['holder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['holder_id' => 'id']],
	        [['info', 'calc_form', 'program', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'api_id' => 'Api',
            'price' => 'Стоимость',
            'status' => 'Статус',
            'holder_id' => 'Владелец',
            'info' => 'Информация',
            'calc_form' => 'Форма заказа',
            'program' => 'Программа',
	        'created_at' => 'Создан',
            'is_police_downloaded' => 'Полис получен'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
	{
		return [
			'serializedAttributes' => [
				'class' => SerializeBehavior::className(),
				'attributes' => ['info', 'calc_form', 'program'],
				'encode' => true,
			],
		];
	}

    /**
     * API
     * @return \yii\db\ActiveQuery
     */
    public function getApi()
    {
        return $this->hasOne(Api::className(), ['id' => 'api_id']);
    }

    /**
     * Валюта
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Владелец
     * @return \yii\db\ActiveQuery
     */
    public function getHolder()
    {
        return $this->hasOne(Person::className(), ['id' => 'holder_id']);
    }

    /**
     * Владелец (компания)
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Название статуса
     * @return mixed|string
     */
    public function getStatusName(){
	    return isset($this->_status_names[$this->status])?$this->_status_names[$this->status]:$this->status;
    }

    /**
     * Ссылка на полис
     * @return bool|string
     */
    public function getPoliceLink(){
	  return $this->api->getModule()->getPoliceLink($this);
    }
}
