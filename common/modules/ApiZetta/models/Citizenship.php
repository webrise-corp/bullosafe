<?php

namespace common\modules\ApiZetta\models;

use Yii;

/**
 * Гражданство
 * This is the model class for table "api_zetta_citizenship".
 *
 * @property int $id
 * @property srting $ext_id
 * @property string $title
 */
class Citizenship extends Classifier {

    /**
     * @inheritdoc
     */
    public static $table_postfix = 'citizenship';

    /**
     * @inheritdoc
     */
    public $title_length = 100;

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend', 'ИД гражданства'),
            'ext_id' => Yii::t('backend', 'ИД гражданства во внешней системе'),
            'title' => Yii::t('backend', 'Гражданство')
        ];
    }

}
