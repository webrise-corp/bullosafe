<?php
/**
 * Copyright (c) kvk-group 2017.
 */

/**
 * Created by PhpStorm.
 * User: miloslawsky
 * Date: 17.10.17
 * Time: 16:55
 */

namespace api\components\Rest\Post;

use api\components\ErrorCode;
use api\components\Rest\RestMethod;
use common\components\ApiModule;
use common\components\Calculator\filters\Filter;
use common\components\Calculator\filters\params\travel\FilterParamPrototype;
use common\components\Calculator\filters\params\travel\FilterParamSum;
use common\components\Calculator\forms\TravelForm;
use common\components\Calculator\models\travel\FilterParam;
use common\models\AdditionalCondition;
use common\models\Api;
use common\models\CostInterval;
use common\models\Currency;
use common\models\GeoCountry;
use common\models\InsuranceType;
use common\models\Risk;
use common\models\RiskCategory;
use common\modules\geo\models\GeoName;

/**
 * Class PreOrderTravel
 *
 * ### Предварительный заказ полиса
 *
 * Тип запроса | Относительный URI | Комментарий
 * --- | --- | ---
 * POST | /pre-order/travel | Предварительный заказ полиса туристического страхования
 *
 * Параметры запроса
 *
 * Ключ | Значение | Обязательный | Комметарий
 * --- | --- | --- | ---
 * calc_id | Идентификатор расчёта | + | Возвращается с данными при расчете стоимости полиса
 * treveller | Массив элементов описывающих путешественника в следующем формате: `{  firstname: "", lastname: "", birthday: "", gender: ""}` |	+ |	Все поля обезательные. Имя и фамилия — латинницей. Дата рождения в формате `Y-m-d`. Пол — `male` или `female`
 * payer |	Структура описывающая плательщика в следующем формате: ` { firstname: "", lastname: "", phone: "", email: "", birthday: "", gender: "", passport: ""} `  | + | Все поля обезательные. Имя и фамилия — латинницей. Дата рождения в формате `Y-m-d`. Пол — `male` или `female`, номер телефона в международном формате
 *
 * Ответ — массив элементов со следующими атрибутами
 *
 * Ключ | Значение
 * --- | ---
 * order_id |	Идентификатор заказа
 * police_url |	URL полиса, если возможно. В противном случае отсутствует
 * price |	Уточненная стоимость полиса
 *
 * @package api\components\Rest\Post
 */
class PreOrderTravel extends RestMethod
{
    /** @inheritdoc */
    public $sort_order = 1010;
    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /** @inheritdoc */
    public function initData()
    {
        parent::initData();
    }

    /** @inheritdoc */
    public function save(){
    }
}