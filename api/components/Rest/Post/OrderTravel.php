<?php
/**
 * Copyright (c) kvk-group 2017.
 */

/**
 * Created by PhpStorm.
 * User: miloslawsky
 * Date: 17.10.17
 * Time: 16:55
 */

namespace api\components\Rest\Post;

use api\components\ErrorCode;
use api\components\Rest\RestMethod;
use common\components\ApiModule;
use common\components\Calculator\filters\Filter;
use common\components\Calculator\filters\params\travel\FilterParamPrototype;
use common\components\Calculator\filters\params\travel\FilterParamSum;
use common\components\Calculator\forms\TravelForm;
use common\components\Calculator\models\travel\FilterParam;
use common\models\AdditionalCondition;
use common\models\Api;
use common\models\CostInterval;
use common\models\Currency;
use common\models\GeoCountry;
use common\models\InsuranceType;
use common\models\Risk;
use common\models\RiskCategory;
use common\modules\geo\models\GeoName;

/**
 * Class OrderTravel
 *
 * ### Подтверждение оплаты полиса
 *
 * Тип запроса | Относительный URI | Комментарий
 * --- | --- | ---
 * POST | /order/travel | Подтверждение оплаты полиса
 *
 * Параметры запроса
 *
 * Ключ | Значение | Обязательный | Комметарий
 * --- | --- | --- | ---
 * order_id | Идентификатор заказа | + | Возвращается с данными при предварительном расчете
 *
 * Ответ — массив элементов со следующими атрибутами
 *
 * Ключ | Значение
 * --- | ---
 * price |	Уточненная стоимость полиса
 * police_url |	URL полиса, если возможно. В противном случае отсутствует
 *
 * @package api\components\Rest\Post
 */
class OrderTravel extends RestMethod
{
    /** @inheritdoc */
    public $sort_order = 1020;
    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    /** @inheritdoc */
    public function initData()
    {
        parent::initData();
    }

    /** @inheritdoc */
    public function save(){
    }
}